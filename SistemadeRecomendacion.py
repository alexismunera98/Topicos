# -*- coding: utf-8 -*-
# Calculo de scores de similitud
# El score de similitud es una medida que permite establecer que tan
# parecidos son dos elementos. Generalmente se trabaja con dos tipos de score
# Euclidiano y Pearson

# Score Euclidiano: utiliza la distancia Euclidiana entre dos puntos para calcular
# el score. El valor de la distancia Euclidiana no esta acotada, sin embargo es recomendable
# normalizarlo en los valores de 0 a 1. Si la distancia es muy grande, el score debe ser pequeño

# Score de Pearson: es una medida de correlacion entre dos objetos. Utiliza la covarianza
# entre los 2 objetos junto con la desviacion estandar de cada uno de ellos para calcular
# el score. El rango del score de Pearson esta acotada entre -1 y 1.
# Un score de +1 indica que los objetos son muy similares, mientras un score de -1 indica que
# son muy diferentes. Un score de 0 indicara que no hay correlación entre ellos

# Filtro colaborativo: es un proceso para identificar patrones dentro de los objetos de un conjunto 
# de datos con el fin de tomar una decicion acerca de un nuevo objeto.
# En el contexto de los sitemas de recomendacion, se utiliza el filtrado colaborativo para mejorar
# las recomendaciones buscando usuarios similares en las bases de datos. Su funcionamiento depende
# del tamaño de la base de datos (se recomienda para bases de datos de gran tamaño)


# importamos las librerias necesarias
import numpy as np
import argparse # sirve para manejar argumentos desde consola
import json

# definimos la funcion que nos permitira manipular los argumentos
def lector_argumentos():
    lector = argparse.ArgumentParser(description='Calculo del Score')
    # introducimos los argumentos que se van a leer
    lector.add_argument('--usuario1', dest='usuario1', required=True, help='Primer usuario')
    lector.add_argument('--usuario2', dest='usuario2', required=True, help='Segundo usuario')
    lector.add_argument('--tipo-score', dest='tipo_score',
                         required=True, choices=['Euclidiano','Pearson'], help='Primer usuario')
    return lector

# definimos la funcion para el calculo del score Euclidiano
def score_euclidiano(datos, usuario1, usuario2):
    #primero nos aseguramos que los usuarios esten en la base de datos
    if usuario1 not in datos:
        raise TypeError('No puedo encontrar al usuario %s en la base de datos' % usuario1)
    if usuario2 not in datos:
        raise TypeError('No puedo encontrar al usuario %s en la base de datos' % usuario2)
    # inicializamos un diccionario con las peliculas comunes
    peliculas_comunes = {}
    # agregamos las peliculas comunes
    for item in datos[usuario1]:
        peliculas_comunes[item]=1
    if len(peliculas_comunes) == 0:
        return 0
    # calculamos la distancia euclidiana para los elementos comunes
    cuadrado_diferencias = [] # inicializamos la lista que almacena las distancias por elemento
    for item in datos[usuario1]:
        if item in datos[usuario2]:
            # calculamos las distancias para cada item comun
            cuadrado_diferencias.append(np.square(datos[usuario1][item] - datos[usuario2][item]))
    # retornamos el inverso de la distancia euclidia 1 / (1+(distancias^1/2))
    return (1/(1 + np.sqrt(np.sum(cuadrado_diferencias))))


# definimos la funcion del score de Pearson
def score_pearson(datos, usuario1, usuario2):
    if usuario1 not in datos:
        raise TypeError('No puedo encontrar al usuario %s en la base de datos' % usuario1)
    if usuario2 not in datos:
        raise TypeError('No puedo encontrar al usuario %s en la base de datos' % usuario2)
    
    # inicializamos el diccionario de las peliculas comunes
    peliculas_comunes = {}
    # hacemos el conteo de los elementos en comun
    for item in datos[usuario1]:
        if item in datos[usuario2]:
            peliculas_comunes[item] = 1
    if len(peliculas_comunes) == 0:
        return 0
    # calculamos la sumatorias de los elementos comunes
    suma_usuario1 = np.sum([datos[usuario1][item] for item in peliculas_comunes])
    suma_usuario2 = np.sum([datos[usuario2][item] for item in peliculas_comunes])
    
    # calculamos la suma de los cuadrados
    cuadrado_usuario1 = np.sum([np.square(datos[usuario1][item]) for item in peliculas_comunes])
    cuadrado_usuario2 = np.sum([np.square(datos[usuario2][item]) for item in peliculas_comunes])
    
    # calculamos la sumatoria de los productos del rating
    suma_productos = np.sum([datos[usuario1][item]*datos[usuario2][item] for item in peliculas_comunes])
    
    # calculamos la correlacion usuario1 vs usuario2
    Sxy = suma_productos - (suma_usuario1*suma_usuario2/len(peliculas_comunes))
    # calulamos la autorrelacion del usuario 1
    Sxx = cuadrado_usuario1 - (np.square(suma_usuario1)/len(peliculas_comunes))
    # calulamos la autorrelacion del usuario 2
    Syy = cuadrado_usuario2 - (np.square(suma_usuario2)/len(peliculas_comunes))
    
    if Sxx*Syy == 0:
        return 0
    return Sxy/(np.sqrt(Sxx*Syy))


if __name__ == '__main__':
    argumentos = lector_argumentos().parse_args() # adquirimos los argumentos
    usuario1 = argumentos.usuario1
    usuario2 = argumentos.usuario2
    tipo_score = argumentos.tipo_score
    # cargamos los archivos de la base de datos
    archivo = 'ratings.json'
    with open(archivo, 'r') as f:
        datos = json.loads(f.read())
    if tipo_score == 'Euclidiano':
        print('\n El score Euclidiano es:')
        print(score_euclidiano(datos,usuario1,usuario2))
    else:
        print('\n El score Euclidiano es:')
        print(score_pearson(datos,usuario1,usuario2))









